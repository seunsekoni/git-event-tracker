<?php

namespace Tests\Feature;

use App\Models\Actor;
use App\Models\Event;
use App\Models\Repo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ActorTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if all actor and their events can be fetched
     *
     * @return void
     */
    public function testIfAllActorsAndTheirEventsCanFetched()
    {
        Actor::factory()->count(1)->create();
        Repo::factory()->count(1)->create();
        Event::factory()->count(20)->create();

        $response = $this->getJson("/api/actors");

        $response->assertJsonFragment([
            [
                'id' => Actor::first()->id,
                'login' => Actor::first()->login,
                'avatar' => Actor::first()->avatar
            ]
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test if an actor's avatar can be updated
     *
     * @return void
     */
    public function testIfAnActorAvatarCanBeUpdated()
    {
        $actor = Actor::factory()->create();

        $avatar = 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png';

        $payload = [
            'id' => $actor->id,
            'avatar' => $avatar
        ];

        $response = $this->putJson("/api/actors", $payload);

        $response->assertJsonFragment([
            [
                'id' => $actor->id,
                'login' => $actor->login,
                'avatar' => $avatar
            ]
        ]);

        $this->assertDatabaseHas('actors', [
            'avatar' => $avatar
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test if a non exixting actor cannot update
     *
     * @return void
     */
    public function testIfNonExistingActorCannotUpdate()
    {
        $avatar = 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png';

        $payload = [
            'id' => 1,
            'avatar' => $avatar
        ];

        $response = $this->putJson("/api/actors", $payload);
        $response->assertStatus(404);
    }
}
