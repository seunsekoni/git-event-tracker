<?php

namespace Tests\Feature;

use App\Models\Actor;
use App\Models\Event;
use App\Models\Repo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EventTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test to assert if an event can be created.
     *
     * @return void
     */
    public function testIfNewEventCanBeCreated()
    {
        $actor = Actor::factory()->create();
        $repo = Repo::factory()->create();
        $payload = [
            'type' => 'PushTestEvent',
            'actor' => $actor->id,
            'repo' => $repo->id,
        ];

        $response = $this->postJson('/api/events', $payload);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'code',
            'success',
            'message',
            'data'
        ]);

        $this->assertDatabaseHas('events', [
            'type' => 'PushTestEvent',
            'actor_id' => $actor->id,
            'repo_id' => $repo->id,
        ]);
    }

    /**
     * Test if a non-existing actor or repo id cannot be used to create an event
     *
     * @return void
     */
    public function testIfYetToBeCreatedActorAndRepoCannotBeUsedToCreateAnEvent()
    {
        $payload = [
            'type' => 'PushTestEvent',
            'actor' => 1,
            'repo' => 1,
        ];
        $response = $this->postJson('/api/events', $payload);
        $response->assertStatus(404);
    }

    /**
     * Test if a specific actor's event can be fetched
     *
     * @return void
     */
    public function testIfAnActorEventCanBeFetched()
    {
        $actor = Actor::factory()->create();
        Repo::factory()->create();
        Event::factory()->create();
        $actorId = $actor->id;

        $response = $this->getJson("/api/events/actors/$actorId");

        $response->assertStatus(200);
    }

    /**
     * Test if a non existing actor's event cannot be fetched
     *
     * @return void
     */
    public function testIfANonExistingActorEventCannotBeFetched()
    {
        Actor::factory()->create();
        Repo::factory()->create();
        Event::factory()->create();
        $actorId = 10;

        $response = $this->getJson("/api/events/actors/$actorId");

        $response->assertStatus(404);
    }

    /**
     * Test if all event can be fetched
     *
     * @return void
     */
    public function testIfAllEventsCanBeFetched()
    {
        Actor::factory()->count(5)->create();
        Repo::factory()->count(10)->create();
        Event::factory()->count(20)->create();

        $response = $this->getJson("/api/events");

        $response->assertStatus(200);

        $this->assertDatabaseCount('events', 20);
    }

    /**
     * Test if all event can be deleted
     *
     * @return void
     */
    public function testIfAllEventsCanBeDeleted()
    {
        Actor::factory()->count(5)->create();
        Repo::factory()->count(10)->create();
        Event::factory()->count(20)->create();

        $response = $this->deleteJson("/api/erase");

        $response->assertStatus(200);

        $this->assertDatabaseCount('events', 0);
    }
}
