<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Models\Actor;
use App\Models\Event;
use App\Models\Repo;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();

        return ResponseBuilder::asSuccess(Response::HTTP_OK)
            ->withData(EventResource::collection($events))
            ->withHttpCode(Response::HTTP_OK)
            ->withMessage('Events fetched successfully')
            ->build();
    }

    /**
     * Get all events by an Actore
     *
     * @param int $actorId
     * @return \Illuminate\Http\Response
     */
    public function getActorsEvent($actorId)
    {
        $events = Event::where('actor_id', $actorId)->get();
        if (!Actor::find($actorId)) {
            throw new ModelNotFoundException();
        }

        return ResponseBuilder::asSuccess(Response::HTTP_OK)
            ->withData(EventResource::collection($events))
            ->withHttpCode(Response::HTTP_OK)
            ->withMessage('Actor\'s events fetched successfully')
            ->build();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event();
        $event->type = $request->type;

        // Check if actor or repo exists
        if (!Actor::find($request->actor) || !Repo::find($request->repo)) {
            throw new ModelNotFoundException();
        }
        $event->actor()->associate($request->actor);
        $event->repo()->associate($request->repo);
        $event->save();

        return ResponseBuilder::asSuccess(Response::HTTP_CREATED)
            ->withData(new EventResource($event))
            ->withHttpCode(Response::HTTP_CREATED)
            ->withMessage('Event Created successfully')
            ->build();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $events = Event::all();
        foreach ($events as $event) {
            $event->delete();
        }
        return ResponseBuilder::asSuccess(Response::HTTP_OK)
            ->withHttpCode(Response::HTTP_OK)
            ->withMessage('Events deleted successfully')
            ->build();
    }
}
