<?php

namespace App\Http\Controllers;

use App\Http\Resources\ActorResource;
use App\Models\Actor;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class ActorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actors = Actor::withCount('events')
            ->orderBy('events_count', 'desc')
            ->orderBy('created_at', 'desc')
            ->orderBy('login', 'asc')
            ->get();
        return ResponseBuilder::asSuccess(Response::HTTP_OK)
            ->withData(ActorResource::collection($actors))
            ->withHttpCode(Response::HTTP_OK)
            ->withMessage('Actors fetched successfully')
            ->build();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $actor = Actor::find($request->id);
        if (!$actor) {
            throw new ModelNotFoundException();
        }
        $actor->avatar = $request->avatar;
        $actor->update();

        return ResponseBuilder::asSuccess(Response::HTTP_OK)
            ->withData(new ActorResource($actor))
            ->withHttpCode(Response::HTTP_OK)
            ->withMessage('Actor\'s details updated successfully')
            ->build();
    }
}
