<?php

namespace App;

class ApiCode
{
    public const SOMETHING_WENT_WRONG = 250;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_BAD_REQUEST = 400;
    public const HTTP_SERVICE_UNAVAILABLE = 503;
    public const HTTP_EXCEPTION = 503;
}
