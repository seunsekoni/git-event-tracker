<?php

namespace Database\Factories;

use App\Models\Actor;
use App\Models\Event;
use App\Models\Repo;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => $this->faker->name
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Event $event) {
            $event->actor()->associate(Actor::inRandomOrder()->first());
            $event->repo()->associate(Repo::inRandomOrder()->first());
        });
    }
}
