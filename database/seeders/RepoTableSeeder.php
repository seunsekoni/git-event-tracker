<?php

namespace Database\Seeders;

use App\Models\Repo;
use Illuminate\Database\Seeder;

class RepoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'name' => 'git-tracker-even',
                'url' => 'https://bitbucket.org/seunsekoni/git-event-tracker.git',
            ],
            [
                'id' => 2,
                'name' => 'ben-x',
                'url' => 'https://github.com/ben-x/express-dataset.git',
            ]
        ];

        foreach ($items as $item) {
            Repo::updateOrCreate([
                'id' => $item['id']
            ], $item);
        }
    }
}
