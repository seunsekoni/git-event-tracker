<?php

namespace Database\Seeders;

use App\Models\Actor;
use Illuminate\Database\Seeder;

class ActorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'login' => 'seunsek',
                'avatar' => 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png',
            ],
            [
                'id' => 2,
                'login' => 'dansek',
                'avatar' => 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png',
            ]
        ];

        foreach ($items as $item) {
            Actor::updateOrCreate([
                'id' => $item['id']
            ], $item);
        }
    }
}
