<?php

use App\Http\Controllers\ActorController;
use App\Http\Controllers\EventController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/actors', [ActorController::class, 'index']);
Route::put('/actors', [ActorController::class, 'update']);
Route::get('/events/actors/{actorId}', [EventController::class, 'getActorsEvent']);
Route::delete('/erase', [EventController::class, 'destroy']);
Route::apiResource('events', EventController::class)->except([
    'show', 'edit', 'update', 'destroy'
]);
